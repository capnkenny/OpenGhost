// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class OpenGhost : ModuleRules
{
	public OpenGhost(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "HeadMountedDisplay",
            "OnlineSubsystem", "OnlineSubsystemUtils", "UMG", "Slate", "SlateCore" });
        DynamicallyLoadedModuleNames.Add("OnlineSubsystemSteam");
    }
}
