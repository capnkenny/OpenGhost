// Fill out your copyright notice in the Description page of Project Settings.

#include "GameModeConfig.h"

FGameModeConfig::FGameModeConfig()
	: myIsTeamBased(false)
	, myWinCondition(EWinCondition::None)
	, myWinThreshold(0)
	, myEnableCheats(false)
	, myReplicationBit(0)
{
}

void FGameModeConfig::EnsureReplication()
{
	myReplicationBit++;
	myDirtyFlag = false;
}
