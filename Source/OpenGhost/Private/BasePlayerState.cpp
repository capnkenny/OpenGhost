// Fill out your copyright notice in the Description page of Project Settings.

#include "BasePlayerState.h"

#include "Net/UnrealNetwork.h"
#include "BaseGameMode.h"
#include "BaseGameState.h"
#include "BasePlayerController.h"
#include "Engine/World.h"

ABasePlayerState::ABasePlayerState()
	: myKills(0)
	, myDeaths(0)
	, myAssists(0)
	, myTeam(ETeamID::Invalid)
	, myOldTeam(ETeamID::Invalid)
{
}

void ABasePlayerState::ScoreKill(ABasePlayerState * Victim, int32 Points)
{
	myKills++;
	ScorePoints(Points);
}

void ABasePlayerState::ScoreDeath(ABasePlayerState * KilledBy, int32 Points)
{
	myDeaths++;
	ScorePoints(Points);
}

void ABasePlayerState::ScoreAssist(ABasePlayerState * Killer, ABasePlayerState * Victim, int32 Points)
{
	myAssists++;
	ScorePoints(Points);
}

void ABasePlayerState::ScoreSuicide(int32 Points)
{
	myDeaths++;
	ScorePoints(Points);
}

void ABasePlayerState::Client_InformAboutKill_Implementation(ABasePlayerState * KillerPlayerState, const UDamageType * KillerDamageType, ABasePlayerState * KilledPlayerState)
{
	if (KillerPlayerState->UniqueId.IsValid())
	{
		for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
		{
			ABasePlayerController* controller = Cast<ABasePlayerController>(*It);
			if (controller && controller->IsLocalController())
			{
				ULocalPlayer* localPlayer = Cast<ULocalPlayer>(controller->Player);
				TSharedPtr<const FUniqueNetId> localID = (localPlayer->GetCachedUniqueNetId(),0);
				if (localID.IsValid() && *localPlayer->GetCachedUniqueNetId() == *KillerPlayerState->UniqueId)
				{
					controller->OnKill(KillerPlayerState, KilledPlayerState, KillerDamageType);
				}
			}
		}
	}
}

void ABasePlayerState::Multicast_BroadcastDeath_Implementation(
	ABasePlayerState * KillerPlayerState, 
	const UDamageType * KillerDamageType, 
	ABasePlayerState * KilledPlayerState, 
	ABasePlayerState* AssistPlayerState)
{
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		ABasePlayerController* controller = Cast<ABasePlayerController>(*It);
		if (controller && controller->IsLocalController())
		{
			controller->OnDeath(KillerPlayerState, this, KillerDamageType, AssistPlayerState);
		}
	}
}

void ABasePlayerState::Multicast_BroadcastSuicide_Implementation(ABasePlayerState* KilledPlayerState, const UDamageType * KillerDamageType)
{
	for (FConstPlayerControllerIterator It = GetWorld()->GetPlayerControllerIterator(); It; ++It)
	{
		ABasePlayerController* controller = Cast<ABasePlayerController>(*It);
		if (controller && controller->IsLocalController())
		{
			controller->OnSuicide(KilledPlayerState, KillerDamageType);
		}
	}
}

void ABasePlayerState::Replicate_TeamChanged()
{
	myOnPlayerSwitchedTeams.Broadcast(this, myOldTeam, myTeam);
}

void ABasePlayerState::ScorePoints(int32 Points)
{
	Score = FMath::Max(static_cast<int32>(Score) + Points, 0);
	ABaseGameMode* gameMode = Cast<ABaseGameMode>(GetWorld()->GetAuthGameMode());
	if (gameMode)
		gameMode->OnPlayerScoreChanged(this, Score);

	ABaseGameState* const gameState = GetWorld()->GetGameState<ABaseGameState>();
	if (gameState && CommonTypes::IsParticipating(myTeam))
		gameState->UpdateTeamScore(myTeam);
}

void ABasePlayerState::CopyProperties(APlayerState* PlayerState)
{
	Super::CopyProperties(PlayerState);

	if(ABasePlayerState* player = Cast<ABasePlayerState>(PlayerState))
	{
		player->myTeam = myTeam;
	}
}

void ABasePlayerState::GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABasePlayerState, myTeam);
	DOREPLIFETIME(ABasePlayerState, myKills);
	DOREPLIFETIME(ABasePlayerState, myDeaths);
	DOREPLIFETIME(ABasePlayerState, myAssists);
}

int ABasePlayerState::GetKills() const
{
	return myKills;
}

int ABasePlayerState::GetDeaths() const
{
	return myDeaths;
}

int ABasePlayerState::GetAssists() const
{
	return myAssists;
}

ETeamID ABasePlayerState::GetTeam() const
{
	return myTeam;
}

void ABasePlayerState::SetTeam(ETeamID TeamID)
{
	myOldTeam = myTeam;
	myTeam = TeamID;

	myOnPlayerSwitchedTeams.Broadcast(this, myOldTeam, myTeam);
}
