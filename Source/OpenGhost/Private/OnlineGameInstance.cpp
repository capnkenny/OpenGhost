// Fill out your copyright notice in the Description page of Project Settings.

#include "OnlineGameInstance.h"

void UOnlineGameInstance::AddMessage(FMessage Message)
{
	myMessages.Add(Message);

	myOnAddedMessage.Broadcast(Message);
}

const TArray<FMessage>& UOnlineGameInstance::GetMessages()
{
	return myMessages;
}
