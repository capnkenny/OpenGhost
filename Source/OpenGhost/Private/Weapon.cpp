#include "Weapon.h"

#include "Components/SkeletalMeshComponent.h"
#include "Engine/World.h"
#include "Kismet/GameplayStatics.h"
#include "Net/UnrealNetwork.h"
#include "BaseCharacter.h"
#include "OpenGhost.h"
#include "Projectile.h"

AWeapon::AWeapon()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = true;

	myMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMeshComponent"));
	myMesh->bCastDynamicShadow = true;
	myMesh->CastShadow = true;
	myMesh->SetIsReplicated(true);

	SetRootComponent(myMesh);

	myMuzzleLocationComponent = CreateDefaultSubobject<USceneComponent>(TEXT("MuzzleLocation"));
	myMuzzleLocationComponent->SetupAttachment(myMesh, TEXT("MuzzleSocket"));

	myHasRoundChambered = false;

	myTPMesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("TPMesh"));
	myTPMesh->SetIsReplicated(false);
	myTPMesh->CastShadow = true;
	myTPMesh->bCastDynamicShadow = true;
	myTPMesh->bOwnerNoSee = true;
	myTPMesh->bOnlyOwnerSee = false;
	myTPMesh->bReceivesDecals = true;
	myTPMesh->SetCollisionObjectType(ECC_Pawn);
	myTPMesh->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	myTPMesh->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	myTPMesh->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);
	myTPMesh->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);

	SetWielder(Cast<ABaseCharacter>(GetOwner()));
}

void AWeapon::BeginPlay()
{
	Super::BeginPlay();

	myAmmunition = myMagazineSize;
	myMagazineCount = myMaxMagazines;
}

void AWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (myIsReloading && HasAuthority())
		UpdateReloadTimer(DeltaTime);
}

void AWeapon::Reset()
{
	Super::Reset();

	Detach();
	Destroy();
}

void AWeapon::Multicast_Equip_Implementation()
{
	Equip();
}

void AWeapon::DelayedEquip()
{
	FTimerHandle TimerHandle;
	GetWorldTimerManager().SetTimer(TimerHandle, this, &AWeapon::Equip, 0.25f, false);
}

void AWeapon::Equip()
{
	if (!myWielder)
	{
		SetWielder(Cast<ABaseCharacter>(GetOwner()));
		DelayedEquip();
		return;
	}

	auto FPMesh = myWielder->GetCharacterMesh(true);
	auto TPMesh = myWielder->GetCharacterMesh(false);
	if (!FPMesh || !TPMesh)
	{
		DelayedEquip();
		return;
	}

	myMesh->AttachToComponent(FPMesh, FAttachmentTransformRules::SnapToTargetNotIncludingScale, myWielder->myWeaponSocketName);
	myMesh->SetRelativeTransform(myFPAttachOffset);
	myTPMesh->AttachToComponent(TPMesh, FAttachmentTransformRules::SnapToTargetNotIncludingScale, myWielder->myWeaponSocketName);
	myTPMesh->SetRelativeTransform(myTPAttachOffset);
}

void AWeapon::SetWielder(ABaseCharacter * Wielder)
{
	myWielder = Wielder;
}

void AWeapon::Detach()
{
	myMesh->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
	myTPMesh->DetachFromComponent(FDetachmentTransformRules::KeepWorldTransform);
	DetachFromActor(FDetachmentTransformRules::KeepWorldTransform);
}

void AWeapon::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(AWeapon, myAmmunition, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(AWeapon, myMagazineCount, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(AWeapon, myHasRoundChambered, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(AWeapon, myReloadTimer, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(AWeapon, myWielder, COND_OwnerOnly);
	
	DOREPLIFETIME(AWeapon, myIsReloading);
}

void AWeapon::Fire(const FVector& TargetLocation)
{
	SpawnFireEffects();

	if (!CanFire())
		return;

	myAmmunition--;
	myHasRoundChambered = myAmmunition > 0;

	// TODO this will need tweaking for high latency situations (client decision only for now)
	if (myWielder->IsLocalClientOwned())
	{
		FVector spawnLocation = myMuzzleLocationComponent->GetComponentLocation();
		FVector direction = TargetLocation - spawnLocation;

		if(HasAuthority())
			SpawnProjectile(spawnLocation, direction);
		else
			Server_SpawnProjectile(spawnLocation, direction);
	}

	if (!HasAuthority())
	{
		Server_Fire(TargetLocation);
		return;
	}
}

void AWeapon::SpawnProjectile(const FVector& StartLocation, const FVector& Direction)
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	SpawnInfo.Owner = this;
	SpawnInfo.Instigator = Instigator;

	auto projectile = GetWorld()->SpawnActor<AProjectile>(myProjectileClass, StartLocation, Direction.Rotation(), SpawnInfo);
	//TODO add launch speed modifiers based on weapon+ammo combination and addons
	projectile->Launch(Direction);
}

void AWeapon::Server_SpawnProjectile_Implementation(const FVector& StartLocation, const FVector& Direction)
{
	SpawnProjectile(StartLocation, Direction);
}

bool AWeapon::Server_SpawnProjectile_Validate(const FVector& StartLocation, const FVector& Direction)
{
	return true;
}

void AWeapon::StartReload()
{
	myIsReloading = true;
	myReloadTimer = 0.0f;

	if (myHasRoundChambered)
	{
		PlayMontageOnWielder(myFPReloadChamberedAnimation, true);
		PlayMontageOnWielder(myTPReloadChamberedAnimation, false);
	}
	else
	{
		PlayMontageOnWielder(myFPReloadAnimation, true);
		PlayMontageOnWielder(myTPReloadAnimation, false);
	}

	if (!HasAuthority())
		Server_StartReload();
}

void AWeapon::Server_StartReload_Implementation()
{
	StartReload();
}

bool AWeapon::Server_StartReload_Validate()
{
	return true;
}

// TODO make use of this
void AWeapon::InterruptReload()
{
	myIsReloading = false;
}

bool AWeapon::CanReload()
{
	return !myIsReloading && myMagazineCount > 0;
}

bool AWeapon::CanFire()
{
	return myAmmunition > 0;
}

void AWeapon::SpawnFireEffects()
{
	if (!CanFire())
	{
		if (myEmptyMagazineSound)
		{
			UGameplayStatics::PlaySoundAtLocation(this, myEmptyMagazineSound, GetActorLocation());
		}
		return;
	}

	if (myFireSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, myFireSound, GetActorLocation());
	}

	if(myWielder->IsLocalClientOwned())
		PlayMontageOnWielder(myFPFireAnimation, true);
	else
		PlayMontageOnWielder(myTPFireAnimation, false);
}

void AWeapon::Server_Fire_Implementation(const FVector& TargetLocation)
{
	Fire(TargetLocation);
}

bool AWeapon::Server_Fire_Validate(const FVector& TargetLocation)
{
	return true;
}

void AWeapon::UpdateReloadTimer(float DeltaTime)
{
	myReloadTimer += DeltaTime;
	if (myReloadTimer >= myReloadTime || (myHasRoundChambered && myReloadTimer >= myReloadTimeChambered))
		FinishReload();
}

void AWeapon::FinishReload()
{
	myIsReloading = false;
	myAmmunition = myMagazineSize;
	if (myHasRoundChambered)
		myAmmunition++;
	myMagazineCount--;
}

void AWeapon::PlayMontageOnWielder(UAnimMontage * Montage, bool PlayOnFPMesh)
{
	if (Montage)
	{
		USkeletalMeshComponent* mesh = myWielder->GetCharacterMesh(PlayOnFPMesh);
		UAnimInstance* animInstance = mesh->GetAnimInstance();
		if (animInstance != nullptr)
		{
			animInstance->Montage_Play(Montage, 1.f);
		}
	}
}

