// Fill out your copyright notice in the Description page of Project Settings.

#include "FunctionLibrary.h"

#include "GameFramework/Actor.h"
#include "BaseGameMode.h"
#include "BaseGameState.h"

ABaseGameMode * UFunctionLibrary::GetGameMode(const AActor* WorldRef)
{
	if (!WorldRef)
		return nullptr;

	if(AGameModeBase* gameMode = WorldRef->GetWorld()->GetAuthGameMode())
		return Cast<ABaseGameMode>(gameMode);

	return nullptr;
}

EWinCondition UFunctionLibrary::GetWinCondition(const AActor * WorldRef)
{
	if (!WorldRef)
		return EWinCondition::Invalid;

	if(ABaseGameState* gameState = WorldRef->GetWorld()->GetGameState<ABaseGameState>())
		return gameState->myModeConfig.myWinCondition;
	return EWinCondition::Invalid;
}

FTimespan UFunctionLibrary::GetCurrentTime()
{
	return GetCurrentDateAndTime().GetTimeOfDay();
}

FDateTime UFunctionLibrary::GetCurrentDateAndTime()
{
	return FDateTime::Now();
}
