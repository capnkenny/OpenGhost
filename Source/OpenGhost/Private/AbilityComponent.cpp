#include "AbilityComponent.h"

#include "Net/UnrealNetwork.h"
#include "BaseCharacter.h"

UAbilityComponent::UAbilityComponent()
{
	PrimaryComponentTick.bCanEverTick = true;

	bAutoActivate = true;
	bReplicates = true;
	bWantsInitializeComponent = true;
}

ABaseCharacter * UAbilityComponent::GetOwningCharacter()
{
	if (!myOwner)
		myOwner = Cast<ABaseCharacter>(GetOwner());
		
	return myOwner;
}

bool UAbilityComponent::IsAbilityActive() const
{
	return myIsActive;
}

void UAbilityComponent::BeginPlay()
{
	Super::BeginPlay();

	GetOwningCharacter();
}

void UAbilityComponent::UpdateAbility(float DeltaTime)
{
	if (!myIsOngoing || !myIsActive)
		return;

	if (!myOwner->UseEnergy(myOngoingCost * DeltaTime))
		DeactivateAbility();
}

void UAbilityComponent::Server_Activate_Implementation()
{
	ActivateAbility();
}

bool UAbilityComponent::Server_Activate_Validate()
{
	return true;
}

void UAbilityComponent::Server_Deactivate_Implementation()
{
	DeactivateAbility();
}

bool UAbilityComponent::Server_Deactivate_Validate()
{
	return true;
}

bool UAbilityComponent::CanActivate() const
{
	return myOwner->GetEnergy() >= myCost && !IsAbilityActive() && myCooldownTimer <= 0.0f;
}

void UAbilityComponent::UpdateCooldownTimer(float DeltaTime)
{
	if (IsAbilityActive() || myCooldownTimer <= 0.0f)
		return;

	myCooldownTimer -= DeltaTime;
}

void UAbilityComponent::UpdateActiveTimer(float DeltaTime)
{
	if (!IsAbilityActive() || myActiveTimer <= 0.0f)
		return;

	myActiveTimer -= DeltaTime;
}

bool UAbilityComponent::HasAuthority()
{
	return myOwner->HasAuthority();
}

void UAbilityComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	UpdateAbility(DeltaTime);
	UpdateActiveTimer(DeltaTime);
	UpdateCooldownTimer(DeltaTime);
}

void UAbilityComponent::InitializeComponent()
{
	Super::InitializeComponent();

	GetOwningCharacter()->AddAbility(this);
}

void UAbilityComponent::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION(UAbilityComponent, myActiveTimer, COND_OwnerOnly);
	DOREPLIFETIME_CONDITION(UAbilityComponent, myCooldownTimer, COND_OwnerOnly);

	DOREPLIFETIME(UAbilityComponent, myIsActive);
}

void UAbilityComponent::TriggerAbility()
{
	if (!IsAbilityActive())
		ActivateAbility();
	else
		DeactivateAbility();
}

void UAbilityComponent::ActivateAbility()
{
	if (!HasAuthority())
	{
		Server_Activate();
		return;
	}

	if (!CanActivate())
		return;	// TODO give user error feedback

	if (!myOwner->UseEnergy(myCost))
		return;

	if (myIsOngoing)
		myIsActive = true;
	else
		myCooldownTimer = myCooldown;

	AbilityActivatedEvent();
	myOnAbilityActivated.Broadcast();
}

void UAbilityComponent::DeactivateAbility()
{
	if (!HasAuthority())
	{
		Server_Deactivate();
		return;
	}

	if (!IsAbilityActive())
		return;

	if (myIsOngoing)
	{
		myIsActive = false;
		myCooldownTimer = myCooldown;
	}

	AbilityDeactivatedEvent();
	myOnAbilityDeactivated.Broadcast();
}

