// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "CommonTypes.generated.h"

namespace Constants
{
	const FString SESSION_SETTING_MIN_PLAYERS = "MinPlayersSession";
}

UENUM(BlueprintType)
enum class ETeamID : uint8
{
	Invalid		UMETA(DisplayName = "Invalid"),
	Spectator	UMETA(DisplayName = "Spectator"),
	None		UMETA(DisplayName = "None"),
	Team1		UMETA(DisplayName = "Team1"),
	Team2		UMETA(DisplayName = "Team2"),
	Count		UMETA(DisplayName = "Count"),
};

class OPENGHOST_API CommonTypes
{
public:

	static bool IsAssigned(ETeamID TeamID);
	static bool IsParticipating(ETeamID TeamID);
	static bool BelongsToTeam(ETeamID TeamID);
	static int32 TeamToInt(ETeamID TeamID);
};

UENUM(BlueprintType)
enum class EWinCondition : uint8
{
	Invalid		UMETA(DisplayName = "Invalid"),
	None		UMETA(DisplayName = "None"),
	Points		UMETA(DisplayName = "Points"),
	Rounds		UMETA(DisplayName = "Rounds"),
	Count		UMETA(DisplayName = "Count"),
};

UENUM(BlueprintType)
enum class EMessageType : uint8
{
	Invalid		UMETA(DisplayName = "Invalid"),
	Server		UMETA(DisplayName = "Server"),
	Match		UMETA(DisplayName = "Match"),
	Team		UMETA(DisplayName = "Team"),
	Private		UMETA(DisplayName = "Private"),
	Command		UMETA(DisplayName = "Command"),
	Count		UMETA(DisplayName = "Count"),
};

USTRUCT(BlueprintType, Blueprintable)
struct FMessage
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Messaging")
	EMessageType myType;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Messaging")
	FString myContent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Messaging")
	int32 mySender;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Messaging")
	FString mySenderName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Messaging")
	int32 myRecipient;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Messaging")
	FString myRecipientName;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Messaging")
	FTimespan myTimeSent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Messaging")
	ETeamID myTeamFilter;
};

UENUM(BlueprintType)
enum class EMatchState : uint8
{
	Invalid				UMETA(DisplayName = "Invalid"),
	EnteringMap			UMETA(DisplayName = "Loading Map"),
	WaitingToStart		UMETA(DisplayName = "Waiting for Players"),
	InProgress			UMETA(DisplayName = "Playing"),
	WaitingPostMatch	UMETA(DisplayName = "Match Over"),
	LeavingMap			UMETA(DisplayName = "Leaving Map"),
	Aborted				UMETA(DisplayName = "Aborted"),
	Warmup				UMETA(DisplayName = "Warmup"),
	PrepareRound		UMETA(DisplayName = "Preparing Round"),
	InRound				UMETA(DisplayName = "InRound"),
	FinishingRound		UMETA(DisplayName = "Finishing Round"),
	Count				UMETA(DisplayName = "Count"),
};

template<typename T>
FString EnumToString(const FString& enumName, const T value)
{
	UEnum* pEnum = FindObject<UEnum>(ANY_PACKAGE, *enumName, true);
	return pEnum ? pEnum->GetNameStringByIndex(static_cast<uint8>(value)) : FString();
}

template<typename T>
FName EnumToFName(const FString& enumName, const T value)
{
	FString string = EnumToString<T>(enumName, value);
	return FName(*string);
}