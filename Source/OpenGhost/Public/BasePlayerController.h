// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CommonTypes.h"
#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BasePlayerController.generated.h"

class ABaseCharacter;
class ABasePlayerState;
class UGameModeConfig;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_FourParams(FPlayerDiedDelegate, ABasePlayerState*, Killer, ABasePlayerState*, Victim, const UDamageType*, DamageType, ABasePlayerState*, Assist);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FPlayerSuicidedDelegate, ABasePlayerState*, Victim, const UDamageType*, DamageType);

UCLASS()
class OPENGHOST_API ABasePlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ABasePlayerController();

	bool IsGameInputAllowed() const;
	void OnKill(ABasePlayerState* KillerPlayerState, ABasePlayerState* KilledPlayerState, const UDamageType* KillerDamageType);
	void OnDeath(ABasePlayerState* KillerPlayerState, ABasePlayerState* KilledPlayerState, const UDamageType* KillerDamageType, ABasePlayerState* AssistPlayerState);
	void OnSuicide(ABasePlayerState* KilledPlayerState, const UDamageType* KillerDamageType);

	//UFUNCTION(BlueprintCallable, Category = "Pawn")
	virtual FVector GetFocalLocation() const override;

	UFUNCTION(BlueprintCallable, Category = "Pawn")
	int32 GetGUID() const;

	UFUNCTION(Client, Reliable, Category = "Controller")
	void Client_NotifyMatchStarted();

	UFUNCTION(Client, Reliable, Category = "Controller")
	void Client_NotifyMatchEnded();

	UFUNCTION(Client, Reliable, Category = "Controller")
	void Client_NotifyStartOnlineGame();

	UFUNCTION(Client, Reliable, Category = "Controller")
	void Client_NotifyEndOnlineGame();

	UFUNCTION(Client, Reliable, Category = "Controller")
	void Client_NotifyRoundEnd(bool bIsWinner, int32 ExpendedTimeInSeconds);

	UFUNCTION(Client, Reliable, Category = "Controller")
	void Client_NotifyRoundStart();

	UFUNCTION(Client, Reliable, Category = "Messaging")
	void Client_ReceiveMessage(FMessage Message);

	UFUNCTION(BlueprintCallable, Server, Reliable, WithValidation, Category = "Messaging")
	void Server_SendMessage(FMessage Message);

	// Intended use for server only
	void NotifyRoundEnd();

	void HandleReturnToMainMenu();
	void CleanupSessionOnReturnToMenu();

	virtual void Tick(float DeltaSeconds) override;
	virtual void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const override;

protected:
	virtual void SetupInputComponent() override; 
	virtual void UnFreeze() override;
	virtual void PreClientTravel(const FString& PendingURL, ETravelType TravelType, bool bIsSeamlessTravel) override;
	virtual void FailedToSpawnPawn() override;
	virtual bool IsMoveInputIgnored() const override;
	virtual bool IsLookInputIgnored() const override;
	virtual void PawnPendingDestroy(APawn* P) override;
	virtual void BeginInactiveState() override;
	virtual void GameHasEnded(class AActor* EndGameFocus = NULL, bool bIsWinner = false) override;
	virtual void Reset() override;

	UFUNCTION(Server, Reliable, WithValidation, Category = "Controller")
	virtual void Server_RespawnRequest();
	virtual void Respawn();


private:
	ABaseCharacter* GetCharacter();
	void StopRespawn();
	void UpdateRespawnTimer();

protected:

	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Controller)
	float myRespawnTimer;
	
private:
	FTimerHandle myRespawnTimerHandle;
	FPlayerDiedDelegate myOnPlayerDied;
	FPlayerSuicidedDelegate myOnPlayerSuicided;
	bool myAllowInputActions;
};
