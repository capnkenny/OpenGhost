#pragma once

#include "GameFramework/Character.h"
#include "BaseCharacter.generated.h"

class AWeapon;
class ABaseCharacter;
class ABasePlayerController;
class UAbilityComponent;
class UCameraComponent;
class USkeletalMeshComponent;
class USoundBase;
class USpringArmComponent;
class UUserWidget;

UENUM(BlueprintType)
enum class ECharacterStateFlag : uint8
{
	None		UMETA(DisplayName = "None"),
	Walk		UMETA(DisplayName = "Walk"),
	Jog			UMETA(DisplayName = "Jog"),
	Sprint		UMETA(DisplayName = "Sprint"),
	Crouch		UMETA(DisplayName = "Crouch"),
	Aim			UMETA(DisplayName = "Aim"),
	Count		UMETA(DisplayName = "Count"),
};

USTRUCT()
struct FTakeHitInfo
{
	GENERATED_USTRUCT_BODY()

public:
	FTakeHitInfo();

	FDamageEvent& GetDamageEvent();
	void SetDamageEvent(const FDamageEvent& DamageEvent);
	void EnsureReplication();

public:
	UPROPERTY()
	float myDamage;

	UPROPERTY()
	UClass* myDamageTypeClass;

	UPROPERTY()
	TWeakObjectPtr<ABaseCharacter> myPawnInstigator;

	UPROPERTY()
	TWeakObjectPtr<AActor> myDamageCauser;

	UPROPERTY()
	int32 myDamageEventClassID;

	UPROPERTY()
	uint32 myKilled : 1;

private:

	UPROPERTY()
	uint8 myEnsureReplicationByte;

	UPROPERTY()
	FDamageEvent myGeneralDamageEvent;

	UPROPERTY()
	FPointDamageEvent myPointDamageEvent;

	UPROPERTY()
	FRadialDamageEvent myRadialDamageEvent;

};

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCharacterWasHitDelegate, FVector, Origin);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FCharacterHitTargetDelegate, bool, WasCritical);

UCLASS()
class OPENGHOST_API ABaseCharacter : public ACharacter
{
	GENERATED_UCLASS_BODY()

public:
	ABaseCharacter();

	virtual void Tick(float DeltaTime) override;
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;
	virtual void PostInitializeComponents() override;
	virtual void Destroyed() override;
	virtual void PawnClientRestart() override;
	virtual void PossessedBy(AController* Controller) override;
	virtual void OnRep_PlayerState() override;
	virtual bool IsReplicationPausedForConnection(const FNetViewer& ConnectionOwnerNetViewer) override;
	virtual void OnReplicationPausedChanged(bool IsReplicationPaused) override;
	virtual void FellOutOfWorld(const UDamageType& DamageType) override;
	virtual void PreReplication(IRepChangedPropertyTracker & ChangedPropertyTracker) override;
	virtual void BeginDestroy() override;
	virtual void Reset() override;

	float TakeDamage(float Damage, const FDamageEvent& DamageEvent, AController* EventInstigator, AActor* DamageCauser);
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	UFUNCTION(BlueprintCallable, Category = Character)
	FORCEINLINE bool IsAlive() const { return myHealth > 0.0f; };

	bool IsLocalClientOwned() const;

	// returns either the FP or TP mesh depending on the callers view of this character
	UFUNCTION(BlueprintCallable, Category = Mesh)
	USkeletalMeshComponent* GetDefaultCharacterMesh() const;

	UFUNCTION(BlueprintCallable, Category = Mesh)
	USkeletalMeshComponent* GetCharacterMesh(bool GetFirstPersonMesh) const;

	// Actions
	void StartFire();
	void StopFire();
	void MoveForward(float Amount);
	void MoveRight (float Amount);
	void Turn(float Amount);
	void LookUp(float Amount);
	void StartCrouch();
	void StopCrouch();
	void ToggleCrouch();
	void StartSprint();
	void StopSprint();
	void ToggleSprint();
	void StartWalk();
	void StopWalk();
	void ToggleWalk();
	void StartAim();
	void StopAim();
	void ToggleAim();
	void StartJump();
	void StopJump();
	void Reload();
	void ActivateAbility();

	UFUNCTION(Client, Reliable)
	void Client_PossessedBy(ABasePlayerController* PlayerController);

	UFUNCTION(BlueprintImplementableEvent, Category = Character)
	void OnPossession();

	UFUNCTION(BlueprintCallable, Category = Movement)
	bool IsAiming() const;

	UFUNCTION(BlueprintCallable, Category = Movement)
	bool IsCrouching() const;

	UFUNCTION(BlueprintCallable, Category = Movement)
	bool IsSprinting() const;

	UFUNCTION(BlueprintCallable, Category = Movement)
	bool IsWalking() const;

	UFUNCTION(BlueprintCallable, Category = Mesh)
	bool IsFirstPerson() const;

	UFUNCTION(BlueprintCallable, Category = Ability)
	bool IsUsingEnergy() const;

	UFUNCTION(BlueprintCallable, Category = Ability)
	bool UseEnergy(float Amount);

	UFUNCTION(BlueprintCallable, Category = Ability)
	float GetEnergy() const;

	UFUNCTION(BlueprintCallable, Category = Character)
	bool IsEnemyOf(AActor* OtherActor) const;

	UFUNCTION(BlueprintImplementableEvent, Category = Character)
	void OnDeath();

	void AddAbility(UAbilityComponent* Ability);

	UFUNCTION(BlueprintCallable, Category = Character)
	bool IsDying() const;

	void SpawnLoadout();
	void EquipDefault();

protected:
	virtual void BeginPlay() override;

	UFUNCTION()
	void Replicate_LastTakenHitInfo();

private:
	void Die(float Damage, AController* Killer, const FDamageEvent& DamageEvent, AActor* DamageCauser);
	bool CanDie() const;
	void CreateHitEffects(float Damage, const FDamageEvent& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser);
	void SetRagdollPhysics();
	virtual void TornOff() override;
	virtual void OnDeath(float Damage, struct FDamageEvent const& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser);
	void ReplicateHit(float Damage, struct FDamageEvent const& DamageEvent, APawn* PawnInstigator, AActor* DamageCauser, bool bKilled);

	bool IsEnemyOf(AController* OtherController) const;
	ABasePlayerController* GetAssistingPlayer(AController * Killer) const;

	void SpawnWeapon(TSubclassOf<AWeapon> WeaponClass, FName Name);

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_SpawnWeapon(TSubclassOf<AWeapon> WeaponClass, FName Name);

	UFUNCTION(Server, Reliable, WithValidation)
	void Server_SetCharacterStateFlag(ECharacterStateFlag Flag, bool State);

	UFUNCTION(Server, Unreliable, WithValidation)
	void Server_UpdateAimRotator(FRotator Rotation);

	void SetSprint(bool IsSprinting);
	void SetCrouch(bool IsCrouching);
	void SetAim(bool IsAiming);
	void SetWalk(bool IsWalking);
	void UpdateCharacterState(ECharacterStateFlag LatestInput);
	void SetHighestPriorityState();

	void CreateIngameUI();
	void DestroyIngameUI();

	UFUNCTION(BlueprintCallable)
	void SetIngameUIVisibility(bool IsVisible);

	UFUNCTION(BlueprintCallable)
	void ToggleIngameUIVisibility();

	void Despawn();
	void UpdateMeshRotation();
	void UpdateMeshVisibility();
	void StopAllMontages();

	void RegenerateEnergy(float DeltaTime);
	void UpdateEnergyRegenerationCooldown(float DeltaTime);
	bool CanRegenerateEnergy() const;

	void Equip(AWeapon* Weapon);

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Character)
	TSubclassOf<AWeapon>  myWeaponClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Character)
	USoundBase*  myDeathSound;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Character)
	FName myWeaponSocketName;

	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadWrite, Category = Character)
	float myHealth;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Character)
	float myMaxHealth;

	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Character)
	AWeapon* myWeapon;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera)
	float myTurnRate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera)
	float myLookUpRate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = UI)
	TSubclassOf<UUserWidget>  myIngameUIWidgetClass;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Character)
	float mySprintSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Character)
	float myJogSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Character)
	float myWalkSpeed;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ability)
	float myEnergyRegenationRate;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ability)
	float myMaxEnergy;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ability)
	float myEnergyRegenerationCooldown;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = Character)
	FCharacterWasHitDelegate myOnCharacterWasHit;

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = Character)
	FCharacterHitTargetDelegate myOnCharacterHitTarget;

protected:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera)
	UCameraComponent* myCamera;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera)
	USpringArmComponent* myCameraArm;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Camera)
	USkeletalMeshComponent* myTPMesh;

	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Movement)
	FRotator myAimRotator;

	// These are flags refering to the input state
	// This doesn't mean this corelates to the action of the character.
	// F.e. if crouching and sprinting is active only the former will take place.
	// Use the Is<Action>() functions for correct state information.
	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Character)
	bool myWantsToAim;
	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Movement)
	bool myIsAiming;

	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Movement)
	bool myWantsToCrouch;
	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Movement)
	bool myIsCrouching;

	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Movement)
	bool myWantsToSprint;
	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Movement)
	bool myIsSprinting;

	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Movement)
	bool myWantsToWalk;
	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Movement)
	bool myIsWalking;

	UPROPERTY(Replicated, VisibleInstanceOnly, BlueprintReadOnly, Category = Ability)
	float myEnergy;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Ability)
	TArray<UAbilityComponent*> myAbilities;

	UPROPERTY(Transient, ReplicatedUsing = Replicate_LastTakenHitInfo)
	struct FTakeHitInfo myLastTakenHitInfo;

	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Category = Character)
	bool myIsDying;

private:
	TMap<ABasePlayerController*, float> myDamageDealers;
	UUserWidget* myIngameUIWidget;
	float myEnergyRegenerationTimer;
	float myLastTakenHitTimeTimeout;
};
