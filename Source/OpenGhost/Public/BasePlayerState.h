// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CommonTypes.h"
#include "CoreMinimal.h"
#include "GameFramework/PlayerState.h"
#include "BasePlayerState.generated.h"

class ABasePlayerState;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FPlayerSwitchedTeamsDelegate, ABasePlayerState*, Player, ETeamID, OldTeam, ETeamID, NewTeam);

UCLASS(BlueprintType, Blueprintable, notplaceable)
class OPENGHOST_API ABasePlayerState : public APlayerState
{
	GENERATED_BODY()

public:
	ABasePlayerState();

	virtual void CopyProperties(class APlayerState* PlayerState) override;
	void GetLifetimeReplicatedProps(TArray< FLifetimeProperty > & OutLifetimeProps) const override;

	void ScoreKill(ABasePlayerState* Victim, int32 Points);
	void ScoreDeath(ABasePlayerState* KilledBy, int32 Points);
	void ScoreAssist(ABasePlayerState* Killer, ABasePlayerState* Victim, int32 Points);
	void ScoreSuicide(int32 Points);

	UFUNCTION(BlueprintCallable)
	int GetKills() const;

	UFUNCTION(BlueprintCallable)
	int GetDeaths() const;

	UFUNCTION(BlueprintCallable)
	int GetAssists() const;

	UFUNCTION(BlueprintCallable)
	ETeamID GetTeam() const;

	UFUNCTION(BlueprintCallable)
	void SetTeam(ETeamID TeamID);

	UFUNCTION(Reliable, Client)
	void Client_InformAboutKill(ABasePlayerState* KillerPlayerState, const UDamageType* KillerDamageType, ABasePlayerState* KilledPlayerState);

	UFUNCTION(Reliable, NetMulticast)
	void Multicast_BroadcastDeath(ABasePlayerState* KillerPlayerState, const UDamageType* KillerDamageType, ABasePlayerState* KilledPlayerState, ABasePlayerState* AssistPlayerState);

	UFUNCTION(Reliable, NetMulticast)
	void Multicast_BroadcastSuicide(ABasePlayerState* KilledPlayerState, const UDamageType* KillerDamageType);

	UFUNCTION()
	void Replicate_TeamChanged();

private:
	void ScorePoints(int32 Points);

public:

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = GameMode)
	FPlayerSwitchedTeamsDelegate myOnPlayerSwitchedTeams;

private:

	UPROPERTY(VisibleInstanceOnly, Transient, Replicated)
	int32 myKills;

	UPROPERTY(VisibleInstanceOnly, Transient, Replicated)
	int32 myDeaths;

	UPROPERTY(VisibleInstanceOnly, Transient, Replicated)
	int32 myAssists;

	UPROPERTY(VisibleInstanceOnly, Transient, ReplicatedUsing = Replicate_TeamChanged)
	ETeamID myTeam;

	UPROPERTY(VisibleInstanceOnly, Transient, Replicated)
	ETeamID myOldTeam;
};
