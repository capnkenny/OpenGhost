#pragma once

#include "GameFramework/Actor.h"
#include "Projectile.generated.h"

class ABaseCharacter;
class UCapsuleComponent;
class UDamageType;
class UProjectileMovementComponent;

UCLASS()
class OPENGHOST_API AProjectile : public AActor
{
	GENERATED_BODY()
	
public:	
	AProjectile();

	virtual void Tick(float DeltaTime) override;
	virtual void Reset() override;
	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const;

	UFUNCTION()
	void OnBeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, 
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult);

	UFUNCTION()
	void OnHit(UPrimitiveComponent* OurComponent, AActor* OtherActor, UPrimitiveComponent* OtherComponent, FVector NormalImpulse, const FHitResult& Hit);
	
	FORCEINLINE UCapsuleComponent* GetCollisionComponent() const { return myCollisionComponent; }
	FORCEINLINE UProjectileMovementComponent* GetProjectileMovement() const { return myProjectileMovementComponent; }

	void Launch(FVector Direction, float LaunchSpeedModifier = 1.0f);

protected:
	virtual void BeginPlay() override;

private:

	float CalculateDamage() const;
	void DealDamage(ABaseCharacter* TargetCharacter, UPrimitiveComponent* TargetComponent, const FHitResult& HitResult);

public:
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Damage)
	float myDamage;

	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = Damage)
	float myKnockbackScale;

	UPROPERTY(EditDefaultsOnly, Category = Damage)
	TSubclassOf<UDamageType> myDamageType;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement)
	UProjectileMovementComponent* myProjectileMovementComponent;

	UPROPERTY(VisibleDefaultsOnly, Category = Collision)
	UCapsuleComponent* myCollisionComponent;

	UPROPERTY(Replicated, EditDefaultsOnly, Category = Visuals)
	UStaticMeshComponent* myMesh;

};
