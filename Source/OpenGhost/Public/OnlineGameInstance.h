// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CommonTypes.h"
#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "OnlineGameInstance.generated.h"

UCLASS()
class OPENGHOST_API UOnlineGameInstance : public UGameInstance
{
	GENERATED_BODY()

	DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FAddedMessageDelegate, FMessage, Message);

public:

	UFUNCTION(BlueprintCallable, Category = "Messaging")
	void AddMessage(FMessage Message);

	UFUNCTION(BlueprintCallable, Category = "Messaging")
	const TArray<FMessage>& GetMessages();

public:

	UPROPERTY(BlueprintAssignable, EditAnywhere, BlueprintReadWrite, Category = GameInstance)
	FAddedMessageDelegate myOnAddedMessage;

private:
	TArray<FMessage> myMessages;
};